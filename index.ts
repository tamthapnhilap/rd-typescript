// index.ts
import * as _ from "lodash"
import * as HelloModule from './hello' // import all module's exports as bundle
import hello from './hello'
import { helloWorld as helloWorldTS } from './hello'

function basicSyntax() {
    let name: string = "misostack"
    let email: string = "misostack.com@gmail.com"
    let online: boolean = false
    let yob: number = 1988

    console.log(name, email, online, yob)

    // casting :: later

    // safety, readability, tooling
    // safetyNumber("abasd") : will raise erro

}

/**
 * Add VAT for price
 *
 * @param {number} price
 * @returns {number}
 */
function safetyNumber(price: number): number {
    const vat: number = 0.1
    return price * vat + price
}

function stringLiteralTypes() {
    let myFavoritePet: string = "dog"
    let sentence: string = `My favorite pet is ${myFavoritePet}`
    console.log(sentence)

    //  Type Aliases and Union Types
    type Species = "dog" | "cat" | "bird"
    // let pet1: Species = "abc" // raise error
    let pet1: Species = "dog"
    let pet2: Species = "cat"
    let pet3: Species = "bird"
}

function tuple() {
    // Declare a tuple type
    let x: [string, number]
    // Initialize it
    x = ["hello", 10] // OK
    // Initialize it incorrectly
    // x = [10, "hello"]; // Error
}

function intersectionTypes() {

}

function constEnum() {
    const enum NinjaActivity {
        Espionage,
        Sabotage,
        Assassination
    }
    // nothing generated if you just defined like this
    let myFavoriteNinjaActivity = NinjaActivity.Espionage
    console.log(myFavoriteNinjaActivity)
    console.log(NinjaActivity["Sabotage"])
    enum Color {
        Red = 1,
        Blue,
        Green
    }
    console.log(Color.Green)
    let colorName: string = Color[1]
    console.log(Color[1])

    let binary: number = 0b10
    let octal: number = 0o10
    let heximal: number = 0x10

    console.log('binary:', binary)
    console.log('octal:', octal)
    console.log('heximal:', heximal)

    // array
    let pigs: number[] = [1, 2, 3]
    console.log(pigs)
    let genericStrings: Array<string> = ["first", "2nd", "3rd"]
    console.log(genericStrings)

    const enum DayOfWeek {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }
    console.log(DayOfWeek.Monday)

    let unsure: any
    unsure = "a"
    console.log(unsure)
    unsure = 1
    console.log(unsure)
    unsure = { "a": 1 }
    console.log(unsure)

    const helloWorld = function (): void {
        console.log('helloWorld')
    }
    helloWorld()
}

// Array
function arrayLearning() {
    // find
    const inventory1 = [
        { name: "apple", quanlity: 2, price: 10000 },
        { name: "orange", quanlity: 10, price: 12000 },
        { name: "lemon", quanlity: 50, price: 1000 }
    ]
    const inventory2 = [
        { name: "khan giay", quanlity: 20, price: 5000 },
        { name: "khan long", quanlity: 10, price: 3000 },
    ]
    let inventory: Array<Object>
    inventory = inventory1.concat(inventory2)
    // sort
    inventory = inventory.sort(function (a: any, b: any): number {
        return a.quanlity - b.quanlity;
    })
    console.log(inventory)
    function testQuanlity(element, index, array): boolean {
        return element.quanlity > 0
    }
    if (inventory.every(testQuanlity)) {
        console.info('passed')
    } else {
        console.error('failed')
    }
    inventory.forEach(function (item: any, index: number, items: any[]): void {
        console.log(`${index + 1}.${item.name}:${item.quanlity}`)
        console.log(items[0])
    })
    console.log(inventory = inventory.map(function (item: any, index: number): any {
        return {
            id: index,
            status: item.quanlity > 10 ? 'worse' : 'fine',
            ...item,
            total: item.quanlity * item.price * 1.1
        };
    }))
    console.log(inventory.filter(function (item: any, index: number) {
        return item.quanlity < 20
    }).map(function (item: any, index: number) {
        return item.name
    }))
    // https://itnext.io/how-to-understand-reduce-d246b7a70f78
    let prevYear = 1000000
    console.log(inventory.reduce(function (prev: any, next: any, index: number) {
        return prev + next.total
    }, prevYear))
    // replace uri string
    const uri_format = 'http://example.local/:category_name/:post_name'
    const replaces = [
        { from: ':category_name', to: 'CatA' },
        { from: ':post_name', to: 'PostA' }
    ]

    console.log(replaces.reduce(function (uri: any, replace: any, index: number): any {
        return uri.replace(replace.to, replace.from)
    }, uri_format))

    // simple redux
    const initStates = {
        count: 0,
        history: []
    }

    function reduceAction(prevState: any, nextState: any) {
        let counter: number
        switch (nextState.action) {
            case 'add':
                counter = nextState.payload.value
                break
            case 'remove':
                counter = -1 * nextState.payload.value
                break
            default:
                counter = 0
        }

        return {
            count: prevState.count + counter,
            history: [...prevState.history, nextState]
        }
    }

    const increase = {
        action: 'add',
        payload: {
            value: 1
        }
    }

    const decrease = {
        action: 'remove',
        payload: {
            value: 1
        }
    }

    const listActions = [increase, increase, decrease]
    console.log(listActions.reduce(reduceAction, initStates))
}

function enumLearning() {
    enum MimeType {
        JPEG = 'image/jpeg',
        PNG = 'image/png',
        PDF = 'application/pdf'
    }
    for (let t in MimeType) {
        if (typeof MimeType[t] != "undefined") {
            console.log(t, MimeType[t])
        }
    }


    enum SuperMarket {
        DIENMAYXANH = <any>["CMT8", "NGUYEN DINH CHIEU"],
        DIENMAYCHOLON = <any>["CMT8"],
        COOPMART = <any>["NGUYEN DINH CHIEU", "HOANG SA"]
    }

    console.log(SuperMarket.DIENMAYXANH)
}

function interfaceLearning() {
    interface linkObj {
        label: string,
        href?: string
    }
    function linkHTML(linkObj: { label: string, href: string }): string {
        return `<a href="${linkObj.href}">${linkObj.label}</a>`
    }
    function linkHTMLWithInterface(linkObj: linkObj): string {
        return `<a href="${linkObj.href ? linkObj.href : '#'}">${linkObj.label}</a>`
    }
    console.log(linkHTML({ label: "interface", href: "https://www.typescriptlang.org/docs/handbook/interfaces.html" }))
    console.log(linkHTMLWithInterface({ label: "interface" }))

    interface Point {
        readonly x: number,
        readonly y: number
    }

    let p1: Point = { x: 1, y: 1 }
    // p1.x = 10// will raise error
    interface SquareConfig {
        color?: string;
        width?: number;
        [propName: string]: any;
    }
    function createSquare(config: SquareConfig): { color: string, width: number } {
        return {
            color: config.color ? config.color : "",
            width: config.width ? config.width : 0
        }
    }

    let mySquare = createSquare({ color: "blue", width: 100 })
    console.log(mySquare)
    // http://www.tutorialsteacher.com/typescript/type-assertion
    // let anotherSquare = createSquare({ width: 100, opacity: 0.5 } as SquareConfig)
    let anotherSquare = createSquare({ width: 100, opacity: 0.5 })
    console.log(anotherSquare)

    // function type

    interface SearchFunc {
        (source: string, subString: string): boolean;
    }

    let mySearch: SearchFunc
    mySearch = function (source: string, subString: string): boolean {
        let result = source.search(subString)
        return result > -1
    }
    console.log(mySearch("the long term", "t"))

    interface Product {
        id: number,
        name: string
    }

    interface Catalogue {
        [index: number]: Product,
        length: number
    }

    let productA: Product = { id: 1, name: "Product A" }
    let productB: Product = { id: 2, name: "Product B" }

    let catalogueA: Catalogue = [productA, productB]
    console.log(catalogueA.length)

}

function functionLearning() {
    // optinal and default parameters : optional params must come after non-optional parameters
    function buildName(firstName: string, lastName?: string): string {
        return lastName ? firstName + lastName : firstName
    }

    console.log(buildName('typescript'))

    // function as parameters
    function foo(barFunc: Function): void {
        barFunc()
    }

    foo(function () {
        console.log('bar')
    })

    function buildURL(url: string, ...ids: number[]): string {
        return url + `${ids.length > 0 ? '?ids=' + ids.join(',') : ''}`
    }

    console.log(buildURL('http://example.local', 1, 2, 15, 100))
}

function classLearning() {
    // base classes
    class Greeter {
        greeting: string
        constructor(message: string) {
            this.greeting = message
        }
        greet(): string {
            return `Hello ${this.greeting}`
        }
    }

    let greeter = new Greeter("world")
    console.log(greeter.greet())

    enum AnimalClass {
        POULTRY,
        LIVESTOCK,
        UNKNOWN
    }
    // inheritance
    class Animal {
        static AnimalClass = AnimalClass
        protected readonly name: string
        constructor(name: string, animalClass?: AnimalClass) {
            this.name = name
        }
        move(distanceInMeters: number = 0) {
            console.log(`${this.name} move ${distanceInMeters}m.`)
        }
    }

    class Dog extends Animal {
        private _animalClass: AnimalClass
        constructor(name: string, animalClass?: AnimalClass) {
            super(name, animalClass)
            this._animalClass = animalClass ? animalClass : Animal.AnimalClass.UNKNOWN
        }
        // get animalClass() {
        //     return AnimalClass[this._animalClass]
        // }
        bark() {
            // this.name = "asd"// will raise error
            console.log(`${this.name} : Woof! Woof!`)
        }
    }

    class Cat extends Animal {
        meow() {
            console.log(`${this.name} : Meow! Meow!`)
        }
    }

    let dog = new Dog("Pub", Animal.AnimalClass.LIVESTOCK)
    dog.bark()
    dog.move(10)
    dog.bark()
    dog.move(1)
    // console.log(dog.animalClass)
    // console.log(dog.name)
    let cat = new Cat("Tom")
    cat.meow()
    cat.move(1)

    abstract class Sample {

    }
    // let sample = new Sample() // error
}

function genericsLearning() {
    // simple
    // function identity(arg: number): number {
    //     return arg
    // }
    // function identity(arg: any): any {
    //     return arg
    // }
    function identity<T>(arg: T): T {
        return arg
    }
    let id = identity<string>("randomeid")
    console.log(id)

    class Person {
        firstName: string
        lastName: string
        constructor(firstName: string, lastName: string) {
            this.firstName = firstName
            this.lastName = lastName
        }

        getFullName() {
            return `${this.firstName + ' ' + this.lastName}`
        }
    }

    class Admin extends Person {

    }

    class Manager extends Person {

    }

    let admin = new Admin('a', 'a')
    let manager = new Manager('a', 'a')

    function personEcho<T extends Person>(person: T): T {
        return person
    }

    let foo = personEcho(admin)
    console.log(foo)
}

function classDecoratorLearning() {
    function addMetaData(target: any) {
        // add metadata
        target.__customMetaData = {
            "somekey": "somevalue"
        }
        return target
    }

    function addModel(model: string) {
        return function (target: any) {
            target.model = model
            return target
        }
    }

    @addMetaData
    class Person {
        private _name: string
        constructor(name: string) {
            this._name = name
        }
        public greet() {
            console.log(`Welcome ${this._name}`)
        }
    }

    function getMetaData(target: any) {
        return target.__customMetaData
    }

    let p = new Person("Typescript")
    p.greet()
    console.log(getMetaData(Person))

    function getModel(target: any) {
        return target.model
    }

    @addModel("Toyota")
    class Car {
        private _name: string
        constructor(name: string) {
            this._name = name
        }
        public greet() {
            console.log(getModel(Car))
            console.log(`Welcome ${this._name}`)
        }
    }

    let c = new Car("MyCar")
    c.greet()

}

function useExternalLearning() {
    console.log(_.padStart("1", 3, "00"))
    HelloModule.hello('typescript')
    HelloModule.helloWorld()
    hello('ts')
    helloWorldTS()
}

(function () {
    basicSyntax()
    stringLiteralTypes()
    intersectionTypes()
    tuple()
    constEnum()
    arrayLearning()
    enumLearning()
    interfaceLearning()
    functionLearning()
    classLearning()
    genericsLearning()
    classDecoratorLearning()
    useExternalLearning()
})()