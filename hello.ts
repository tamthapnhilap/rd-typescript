// hello.ts
export function hello(name: string) {
    console.log(`Hello ${name}`)
}

function helloWorld() {
    console.log('Hello World!')
}

export { helloWorld }
export default hello